from flask import Flask,request,render_template,session,redirect,make_response
from flask_sqlalchemy import SQLAlchemy
from form import formvalidation
import math
import re
import pgn
app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///stdu.db'
db=SQLAlchemy(app)
app.secret_key='stf'
class student(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(100))
    fname=db.Column(db.String(100))
    dob=db.Column(db.String(10))
    address=db.Column(db.String(200))
    classs=db.Column(db.String(100))
    school=db.Column(db.String(100))
    gender=db.Column(db.String(100))
    streem=db.Column(db.String(100))
    mobile=db.Column(db.String(100))
    email=db.Column(db.String(100))
@app.route("/",methods=["GET","POST"])
def stu():
    emailvl= '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    error=""
    form=formvalidation()
    if request.method=="POST":
        dob=request.form.get("dob")
        df = str(dob[0:4])
        if form.validate==True:
            flash("Form Submited")
        if dob=="":
            doberror="Enter Date of Birth"
            return render_template("newstwt.html", form=form, doberror=doberror)
        if 2001<=int(df):
            doberror="Should be within the year 2001"
            return render_template("newstwt.html", form=form, doberror=doberror)
        if form.gender.data != "male" and form.gender.data !="female":
            gerror="Enter your gender"
            return render_template("newstwt.html",form=form,gerror=gerror)
        if form.streem.data=="select":
            serror = "What is your stream"
            return render_template("newstwt.html", form=form, serror=serror)
        if not (re.search(emailvl,form.email.data)):
            eerror="Invalid Email"
            return render_template("newstwt.html", form=form, eerror=eerror)
        if len(form.mobile.data) != 10 and form.mobile.data != int:
            merror = "Invalid Mobile number"
            return render_template("newstwt.html", form=form, merror=merror)
        data=db.session.query(student).all()
        for i in data:
            n=str(i.mobile)
            m=str(i.email)
            if form.mobile.data in n:
                error = "Mobile is already registered"
                return render_template("newstwt.html", form=form, merror=error)
            if form.email.data in m:
                error = "Email is already registered"
                return render_template("newstwt.html", form=form, eerror=error)
        a=student(name=(form.name.data).upper(),fname=form.fname.data.upper(),dob=dob,address=form.address.data.upper(),school=form.school.data.upper(),classs=form.classs.data.upper(),gender=form.gender.data.upper(),streem=form.streem.data.upper(),mobile=form.mobile.data,email=form.email.data.upper())
        db.session.add(a)
        db.session.commit()
        b=student.query.filter_by(mobile=form.mobile.data).first()
        alert= f"Student Enroll/roll no is {b.id} ADD SUCCESFULLY"
        a = student.query.filter_by().all()
        return pgn.pgn(a=a,alert=alert,s='')
    return render_template("newstwt.html",form=form)
@app.route("/student_list")
def slist():
    if request.method=="POST":
        a=student.query.filter_by().all()
        alert ="STUDENT ADD SUCCESFULLY"
        return render_template("slist.html",a=a,alert=alert)
    a=student.query.filter_by().all()
    return pgn.pgn(a,alert='',s='')
@app.route("/delete/<string:s_slug>")
def delete(s_slug):
    b=student.query.filter_by(id=s_slug).first()
    db.session.delete(b)
    db.session.commit()
    return redirect("/student_list")
@app.route ("/update/<string:s_slug>",methods=["GET","POST"])
def update(s_slug):
    b=student.query.filter_by(id=s_slug).first()
    error=""
    if request.method=="POST":
        name=request.form.get("name")
        fname=request.form.get("fname")
        dob=request.form.get("dob")
        address=request.form.get("address")
        classs=request.form.get("classs")
        school=request.form.get("school")
        gender=request.form.get("gender")
        streem=request.form.get("streem")
        mobile=request.form.get("mobile")
        email=request.form.get("email")
        if name=="":
            error="Enter your name"
            return render_template("newedit.html",error=error,b=b)
        if fname=="":
            error="Enter your father name"
            return render_template("newedit.html",error=error,b=b)
        if dob=="":
            error="Enter your date of birth"
            return render_template("newedit.html",error=error,b=b)
        if gender != "male" and gender!="female":
            error="Enter your gender"
            return render_template("newedit.html",error=error,b=b)
        if streem=="All":
            error="Enter your streem"
            return render_template("newedit.html",error=error,b=b)
        if email=="":
            error="Enter your email"
            return render_template("newedit.html",error=error,b=b)
        if mobile=="":
            error="Enter your mobile number"
            return render_template("newedit.html",error=error,b=b)
        if address=="":
            error="Enter your address"
            return render_template("newedit.html",error=error,b=b)
        if classs=="":
            error="Enter your class"
            return render_template("newedit.html",error=error,b=b)
        if school=="":
            error="Enter your school"
            return render_template("newedit.html",error=error,b=b)
        b.name=name
        b.fname=fname
        b.dob=dob
        b.address=address
        b.classs=classs
        b.school=school
        b.gender=gender
        b.streem=streem
        b.mobile=mobile
        b.email=email
        db.session.commit()
        a=student.query.filter_by().all()
        return render_template("slist.html",a=a,error=error)
    return render_template("newedit.html",b=b)
@app.route("/search",methods=["GET","POST"])
def search():
    alert="Data not found...."
    error=""
    dbt=''
    if request.method=="POST":
        s=request.form.get("search")
        s=s.upper()
        if s=="":
            return redirect("/student_list")
        data=db.session.query(student).all()
        for i in data:
            if s in i.name:
                dbt=student.query.filter(student.name.like(f"%{s}%")).all()
                alert=f"{t} is {len(dbt)} search.."
            if s in i.fname:
                dbt=student.query.filter(student.fname.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            if s in i.dob:
                dbt=student.query.filter(student.dob.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            if s in i.address:
                dbt=student.query.filter(student.address.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            if s in i.classs:
                dbt=student.query.filter(student.classs.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            if s in i.school:
                dbt=student.query.filter(student.school.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            strm=str(i.streem)
            if s in strm:
                dbt=student.query.filter(student.streem.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            n = str(i.mobile)
            m = str(i.email)
            if s in n:
                dbt=student.query.filter(student.mobile.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
            if s in m:
                dbt=student.query.filter(student.email.like(f"%{s}%")).all()
                alert=f"{s} is {len(dbt)} search.."
        return pgn.pgn(a=dbt,alert=alert,s=s)
    return pgn.pgn(a=dbt, alert=alert,s=s)
app.run(debug=True)

    