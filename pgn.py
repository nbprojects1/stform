import math
from flask import Flask,request,render_template
def pgn(a,**kwarg):
    last = math.ceil(len(a) / 10)
    page = request.args.get('page')
    if (not str(page).isnumeric()):
        page = 1
    page = int(page)
    a = a[(page - 1) * 10:(page - 1) * 10 + 10]
    if page == 1:
        prev = "#"
        next = "/student_list?page=" + str(page + 1)
    elif page == last:
        prev = "/student_list?page=" + str(page - 1)
        next = "#"
    else:
        prev = "/student_list?page=" + str(page - 1)
        next = "/student_list?page=" + str(page + 1)
    alert=kwarg['alert']
    s=kwarg['s']

    return render_template("slist.html", a=a, prev=prev, next=next,alert=alert,s=s)