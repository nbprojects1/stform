from flask_wtf import Form
from wtforms import StringField,RadioField,SubmitField,validators,TextAreaField,SelectField

class formvalidation(Form):
    name=StringField("Student Name",[validators.DataRequired()])
    fname=StringField("Father Name",[validators.DataRequired()])
    #dob=StringField("Date of Birth",[validators.DataRequired()])
    address=TextAreaField("Address",[validators.DataRequired()])
    classs=StringField("Class",[validators.DataRequired()])
    school=StringField("School",[validators.DataRequired()])
    gender=RadioField("Gender",choices = [('male','Male'),('female','Female')])
    streem=SelectField("Stream",choices = [('select','Select'),('maths', 'Maths'),('bio', 'Bio'),('commers','Commers'),('arts','Arts')])
    mobile=StringField("Mobile Number",[validators.DataRequired()])
    email=StringField("Email Id",[validators.DataRequired()])